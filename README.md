Infinite scroll ile tüm apiyi çekip listeleme, navigation barda top20 ve favorilere ekleme basit şekliyle yapılmıştır.

Projede dependency ınjection için koin kullanıldı. Mvvm temel anlamda göstermelik kullanıldı.

Navigation Architecture component ile tek bir activity üzerinden fragmentler oluşturularak hazırlandı.

Zaman olursa tasarım değiştirilip databinding, livedata ve search kısmı eklenebilir. 
