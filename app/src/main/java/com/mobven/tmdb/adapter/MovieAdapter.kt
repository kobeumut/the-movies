package com.mobven.tmdb.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.mobven.tmdb.R
import com.mobven.tmdb.model.Movies
import kotlinx.android.synthetic.main.inflate_list.view.*

class MovieAdapter(private val movies: MutableList<Movies>, val clickListener:(Movies)->Unit) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.inflate_list, parent, false)
        return MovieViewHolder(v)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    fun add(item: Movies, position: Int) {
        movies.add(position, item)
        notifyItemInserted(position)
    }

    fun remove(item: Movies) {
        val position = movies.indexOf(item)
        movies.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]
        holder.itemView.tvName.text = movie.title
        holder.itemView.tvId.text = movie.id.toString()
        holder.itemView.tvDesc.text = movie.overview
        holder.itemView.tvDate.text = movie.release_date
        holder.itemView.setOnClickListener {
            clickListener(movie)
        }

    }
    class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}