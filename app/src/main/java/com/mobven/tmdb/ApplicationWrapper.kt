package com.mobven.tmdb

import android.app.Application
import android.util.Log
import com.mobven.tmdb.api.ApiInterface
import com.mobven.tmdb.di.ImageModule
import com.mobven.tmdb.viewmodel.AllMoviesViewModel
import com.mobven.tmdb.viewmodel.FavoriteMoviesViewModel
import com.mobven.tmdb.viewmodel.TopMoviesViewModel
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApplicationWrapper : Application() {
    private val appModule = module {
        single { ImageModule(get()) }
    }
    private val viewModule = module {
        viewModel { AllMoviesViewModel(get()) }
        viewModel { TopMoviesViewModel(get()) }
        viewModel { FavoriteMoviesViewModel(get()) }
    }
    private val networkModule = module {
        factory<Interceptor> {
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Log.d("API", it) })
                .setLevel(HttpLoggingInterceptor.Level.HEADERS)
        }

        factory { OkHttpClient.Builder().addInterceptor(get()).build() }

        single {
            Retrofit.Builder()
                .client(get())
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        factory { get<Retrofit>().create(ApiInterface::class.java) }
    }


    companion object {
        lateinit var INSTANCE: ApplicationWrapper
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        startKoin {
            androidContext(this@ApplicationWrapper)
            modules(appModule, viewModule, networkModule)
        }

    }
}