package com.mobven.tmdb

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private val navController: NavController by lazy {
        Navigation.findNavController(
            this@MainActivity,
            R.id.my_nav_host_fragment
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setupWithNavController(navController)
//        navController.addOnDestinationChangedListener { _, destination, _ ->
//            when {
//                destination.id == R.id.topMoviesFragment -> if (navView.selectedItemId != R.id.topMoviesFragment) {
//                    navView.selectedItemId = R.id.topMoviesFragment
//                }
//                destination.id == R.id.favoriteMovies -> if (navView.selectedItemId != R.id.favoriteMovies) {
//                    navView.selectedItemId = R.id.favoriteMovies
//                }
//                destination.id == R.id.allMoviesFragment -> if (navView.selectedItemId != R.id.allMoviesFragment) {
//                    navView.selectedItemId = R.id.allMoviesFragment
//                }
//            }
//
//        }
        NavigationUI.setupActionBarWithNavController(this@MainActivity, navController)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val navigated = NavigationUI.onNavDestinationSelected(item!!, navController)
        return navigated || super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController, null)
    }
}
