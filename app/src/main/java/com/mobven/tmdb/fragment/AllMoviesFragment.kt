package com.mobven.tmdb.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobven.tmdb.ApplicationWrapper
import com.mobven.tmdb.MainActivity
import com.mobven.tmdb.R
import com.mobven.tmdb.adapter.MovieAdapter
import com.mobven.tmdb.model.Movies
import com.mobven.tmdb.util.EndlessScroll
import com.mobven.tmdb.util.ListItemDecorations
import com.mobven.tmdb.util.put
import com.mobven.tmdb.viewmodel.AllMoviesViewModel
import kotlinx.android.synthetic.main.all_movies_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class AllMoviesFragment : Fragment() {


    private var allMoviesList: MutableList<Movies>? = null
    private var searchedMoviesList: MutableList<Movies>? = null
    private var pageCount = 1
    private val viewModel: AllMoviesViewModel by viewModel()
    private val sharedPreferences by lazy { ApplicationWrapper.INSTANCE.getSharedPreferences("MOBVEN", 0) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.all_movies_fragment, container, false)
    }

    private lateinit var myCustomAdapter: MovieAdapter
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onRecyclerScrolled()
//        viewModel = ViewModelProviders.of(this).get(AllMoviesViewModel::class.java)
        viewModel.getMovies { movies ->
            allMoviesList = movies?.results
            allMoviesList?.let { list ->
                sharedPreferences.put("pageCount", movies?.total_pages)
                context?.let {
                    if (recyclerView != null) {
                        recyclerView.layoutManager = LinearLayoutManager(context)
                        recyclerView.addItemDecoration(ListItemDecorations(20))
                        recyclerView.setHasFixedSize(true)
                        myCustomAdapter = MovieAdapter(list) {
                            Navigation.findNavController(view!!).navigate(
                                R.id.action_global_movieDetailFragment,
                                bundleOf("movie" to it)
                            )
                        }
                        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                        recyclerView.adapter = myCustomAdapter
                        progressBar.visibility = View.GONE
                    }

                }
            }

        }

    }


    private fun onRecyclerScrolled() {
        recyclerView.addOnScrollListener(object : EndlessScroll() {
            override fun onLoadMore() {
                viewModel.getMovies(pageCount) { movies ->
                    if (pageCount <= sharedPreferences.getInt("pageCount", 1)) {
                        movies?.results?.let { allMoviesList?.addAll(it) }
                        recyclerView.adapter?.notifyDataSetChanged()
                    }

                }


            }


        })
    }
}
