package com.mobven.tmdb.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobven.tmdb.MainActivity
import com.mobven.tmdb.R
import com.mobven.tmdb.adapter.MovieAdapter
import com.mobven.tmdb.model.Movies
import com.mobven.tmdb.util.ListItemDecorations
import com.mobven.tmdb.viewmodel.TopMoviesViewModel
import kotlinx.android.synthetic.main.all_movies_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TopMoviesFragment : Fragment() {


    private val viewModel: TopMoviesViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.all_movies_fragment, container, false)
    }

    private lateinit var myCustomAdapter: MovieAdapter
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getMovies { movies ->
            var listOfMovies: MutableList<Movies> = movies?.results!!
            context?.let {
                if(recyclerView!=null){
                    recyclerView.layoutManager = LinearLayoutManager(context)
                    recyclerView.addItemDecoration(ListItemDecorations(20))
                    recyclerView.setHasFixedSize(true)
                    myCustomAdapter = MovieAdapter(listOfMovies){
                        Navigation.findNavController(view!!).navigate(R.id.action_global_movieDetailFragment,
                            bundleOf("movie" to it))
                    }
                    recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                    recyclerView.adapter = myCustomAdapter
                    progressBar.visibility = View.GONE
                }
            }
        }

    }
}
