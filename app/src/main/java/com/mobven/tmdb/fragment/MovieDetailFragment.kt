package com.mobven.tmdb.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mobven.tmdb.ApplicationWrapper
import com.mobven.tmdb.MainActivity
import com.mobven.tmdb.R
import com.mobven.tmdb.di.ImageModule
import com.mobven.tmdb.model.Movies
import com.mobven.tmdb.util.get
import com.mobven.tmdb.util.put
import com.mobven.tmdb.viewmodel.MovieDetailViewModel
import kotlinx.android.synthetic.main.movie_detail_fragment.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MovieDetailFragment : Fragment() {

    private lateinit var viewModel: MovieDetailViewModel
    private val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500";
    private val imageModule: ImageModule by inject { parametersOf(this@MovieDetailFragment) }
    private var isFavorite: Boolean = false
    private lateinit var movies: Movies
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        movies = arguments?.get("movie") as Movies
        (activity as MainActivity).supportActionBar?.title = movies.title
        return inflater.inflate(R.layout.movie_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MovieDetailViewModel::class.java)
        text.text = movies.overview
        imageModule.get(IMAGE_BASE_URL + movies.poster_path, imageView)
        val sharedPreferences = ApplicationWrapper.INSTANCE.getSharedPreferences("MOBVEN", 0)
        var favoriteList = sharedPreferences.get<MutableList<Movies>>("SAVED_LIST")
        if (favoriteList == null) {
            favoriteList = mutableListOf()
        }
        isFavorite = favoriteList?.contains(movies)

        imageView2.setImageResource(if (isFavorite) android.R.drawable.star_big_on else android.R.drawable.star_big_off)

        imageView2.setOnClickListener {
            if (isFavorite) {
                favoriteList?.remove(movies)
            } else {
                favoriteList?.add(movies)
            }
            isFavorite = !isFavorite
            imageView2.setImageResource(if (isFavorite) android.R.drawable.star_big_on else android.R.drawable.star_big_off)

            sharedPreferences.put("SAVED_LIST", favoriteList)
        }

    }

}
