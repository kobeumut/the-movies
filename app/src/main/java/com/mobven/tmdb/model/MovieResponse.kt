package com.mobven.tmdb.model

data class MovieResponse(
    var page: Int?=null,
    var total_results: Int?=null,
    var total_pages: Int?=null,
    var results: MutableList<Movies>?
)