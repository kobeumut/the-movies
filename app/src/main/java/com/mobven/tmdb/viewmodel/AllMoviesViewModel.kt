package com.mobven.tmdb.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.mobven.tmdb.api.ApiInterface
import com.mobven.tmdb.model.MovieResponse
import org.koin.android.ext.android.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AllMoviesViewModel(application: Application) : AndroidViewModel(application) {
    private val api_key = "b59bded1e1118d5fcd2505d9aa73d3b0"
    private val apiServices:ApiInterface by application.inject<ApiInterface>()
    fun getMovies(page:Int=1, callback:(MovieResponse?)->Unit){
        apiServices.getAllMovies(api_key,page).enqueue(object : Callback<MovieResponse> {
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                return callback(null)
            }

            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                return callback(response.body())
            }

        })
    }
}
