package com.mobven.tmdb.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.mobven.tmdb.ApplicationWrapper
import com.mobven.tmdb.model.MovieResponse
import com.mobven.tmdb.model.Movies
import com.mobven.tmdb.util.get

class FavoriteMoviesViewModel(application: Application) : AndroidViewModel(application) {
    fun getMovies(callback: (MovieResponse?) -> Unit) {
        val sharedPreferences = ApplicationWrapper.INSTANCE.getSharedPreferences("MOBVEN", 0)
        val favoriteList = sharedPreferences.get<MutableList<Movies>>("SAVED_LIST")
        callback(MovieResponse(results = favoriteList))
    }
}
