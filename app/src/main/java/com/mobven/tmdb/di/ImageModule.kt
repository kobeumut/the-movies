package com.mobven.tmdb.di


import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import com.mobven.tmdb.ApplicationWrapper
import com.squareup.picasso.Callback
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.io.File
import java.util.concurrent.TimeUnit


class ImageModule(context: Context? = null) {

    private val picasso_cache = "picasso-cache"
    private val cacheDir = File(ApplicationWrapper.INSTANCE.cacheDir, picasso_cache)
    private val cacheSize: Long = 500 * 1024 * 1024 // 500 MB
    private val cache = Cache(cacheDir, cacheSize)
    private val requestList = mutableListOf<String>()

    private val picasso by lazy {
        val okHttpClient = OkHttpClient().newBuilder()
        okHttpClient.connectTimeout(30, TimeUnit.SECONDS)
        okHttpClient.readTimeout(30, TimeUnit.SECONDS)


        okHttpClient.cache(cache)
        val okHttp3Downloader = OkHttp3Downloader(okHttpClient.build())
        context?.let {
            Picasso.Builder(context)
                .downloader(okHttp3Downloader).build()
        }
    }

    fun get(url: String, target: ImageView, callback: ((result: Boolean) -> Unit)? = null) {
        requestList.add(url)
        picasso?.load(url)?.tag(url)?.fit()?.centerCrop()?.into(target, object : Callback {
            override fun onSuccess() {
                requestList.remove(url)
                callback?.let { it(true) }
            }

            override fun onError(e: Exception?) {
                requestList.remove(url)
                callback?.let { it(false) }
            }
        })
    }

    fun get(res: Int, target: ImageView, callback: ((result: Boolean) -> Unit)? = null) {
        requestList.add(res.toString())
        picasso?.load(res)?.tag(res)?.fit()?.centerCrop()?.into(target, object : Callback {
            override fun onSuccess() {
                requestList.remove(res.toString())
                callback?.let { it(true) }

            }

            override fun onError(e: Exception?) {
                requestList.remove(res.toString())
                callback?.let { it(false) }

            }
        })
    }

    fun getBitmap(url: String): Bitmap? {
        return picasso?.load(url)?.get()
    }

    fun destroy() {
        requestList.forEach {
            cancelRequest(tag = it)
        }
    }

    fun cancelRequest(target: ImageView? = null, tag: String? = null) {
        target?.let {
            picasso?.cancelRequest(it)
        }
        tag?.let {
            picasso?.cancelTag(it)
        }
    }

    fun clearCache() {
        cache?.evictAll()
    }

}