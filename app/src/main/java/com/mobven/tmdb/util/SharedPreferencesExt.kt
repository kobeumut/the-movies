package com.mobven.tmdb.util

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlin.reflect.KClass


val gson: Gson = Gson()

inline fun <reified T:Any> SharedPreferences.get(key: String, defaultValue: KClass<T>?=null): T? {
    when(T::class) {
        Boolean::class -> return this.getBoolean(key, defaultValue as Boolean) as T
        Float::class -> return this.getFloat(key, defaultValue as Float) as T
        Int::class -> return this.getInt(key, defaultValue as Int) as T
        Long::class -> return this.getLong(key, defaultValue as Long) as T
        String::class -> return this.getString(key, defaultValue as String) as T
        else -> {
            return if (defaultValue is Set<*>) {
                this.getStringSet(key, defaultValue as Set<String>) as T
            }else{
                gson.fromJson<T>(this.getString(key,""), object : TypeToken<T>() {}.type)
            }
        }
    }

    return null
}

inline fun <reified T> SharedPreferences.put(key: String, value: T) {
    val editor = this.edit()

    when(T::class) {
        Boolean::class -> editor.putBoolean(key, value as Boolean)
        Float::class -> editor.putFloat(key, value as Float)
        Int::class -> editor.putInt(key, value as Int)
        Long::class -> editor.putLong(key, value as Long)
        String::class -> editor.putString(key, value as String)
        else -> {
            if (value is Set<*>) {
                editor.putStringSet(key, value as Set<String>)
            }else{
                editor.putString(key, gson.toJson(value))
            }
        }
    }

    editor.apply()
}